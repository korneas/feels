var controller = function controller(model, view) {
    var info = this.model;
    view.preload();
    view.menuAction();
    setInterval(function () {
        view.changeNav();
        view.setFilterEvent();
    }, 200);
    
    view.onFilter = function onFilter(validacion){
        var newItems = info.items
        .filter(function(elemento){
            //Filtro placentero
            if(validacion[0]) return elemento.pleasant;
            if(validacion[1]) return !elemento.pleasant;
            if(!validacion[0] && !validacion[1]) return true;
        })
        .filter(function(elemento){
            //Filtro de precio
            if(validacion[2]) return elemento.price<1000;
            if(validacion[3]) return elemento.price>1000 && elemento.price<=4000;
            if(validacion[4]) return elemento.price>4000 && elemento.price<=8000;
            if(validacion[5]) return elemento.price>8000;
            if(!validacion[2] && !validacion[3] && !validacion[4] && !validacion[5]) return true;
        })
        .filter(function(elemento){
            //Filtro de percepción
            if(validacion[6]) return elemento.sense.includes('visual');
            if(validacion[7]) return elemento.sense.includes('listen');
            if(validacion[8]) return elemento.sense.includes('touch');
            if(!validacion[6] && !validacion[7] && !validacion[8]) return true;
        })
        .filter(function(elemento){
            //Filtro de intensidad
            if(validacion[9]) return elemento.intenese == 1;
            if(validacion[10]) return elemento.intenese == 2;
            if(validacion[11]) return elemento.intenese == 3;
            if(!validacion[9] && !validacion[10] && !validacion[11]) return true;
        });
        
        view.render(newItems,info.advertising);
    };
    
    view.render(info.items,info.advertising);
    view.setHeaderEvent();
}

var model = createModel();
controller(model, view);

setInterval(function () {
        console.log($(window).width());
    }, 200);
