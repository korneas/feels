var createModel = function createModel() {
    var info = {
        
        advertising: [
            {
                name: 'Timeless',
                url: 'img/forest.jpg',
                texto: 'This new pack of feelings want to introduce young and dependent love in our list, to express how a world full of strange experiences and drugs may be your last minutes alive. <b><a href="#" style="text-decoration: none; color: white;">See more</a></b>',
                price: 3200
            },
            {
                name: 'Sputnik',
                url: 'img/couple.jpg',
                texto: 'This new pack of feelings want to introduce young and dependent love in our list, to express how a world full of strange experiences and drugs may be your last minutes alive. <b><a href="#" style="text-decoration: none; color: white;">See more</a></b>',
                price: 5700
            },
            {
                name: 'Ghost',
                url: 'img/street.jpg',
                texto: 'This new pack of feelings want to introduce young and dependent love in our list, to express how a world full of strange experiences and drugs may be your last minutes alive. <b><a href="#" style="text-decoration: none; color: white;">See more</a></b>',
                price: 4500
            }
        ],
        
        items: [
            {
                // 1
                name: 'Hostile',
                price: 1900,
                url: 'img/feelings/feel1.jpg',
                sense: 'touch',
                pleasant: false,
                intenese: 2
            },
            {
                // 2
                name: 'Ease',
                price: 5500,
                url: 'img/feelings/feel2.jpg',
                sense: 'touch',
                pleasant: true,
                intenese: 1
            },
            {
                // 3
                name: 'Bitter',
                price: 7400,
                url: 'img/feelings/feel3.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 1
            },
            {
                // 4
                name: 'Sunny',
                price: 1800,
                url: 'img/feelings/feel4.jpg',
                sense: 'listen',
                pleasant: true,
                intenese: 1
            },
            {
                // 5
                name: 'Despair',
                price: 6500,
                url: 'img/feelings/feel5.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 3
            },
            {
                // 6
                name: 'Wonderful',
                price: 8800,
                url: 'img/feelings/feel6.jpg',
                sense: 'touch',
                pleasant: true,
                intenese: 2
            },
            {
                // 7
                name: 'Pain',
                price: 2100,
                url: 'img/feelings/feel7.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 3
            },
            {
                // 8
                name: 'Pleased',
                price: 8000,
                url: 'img/feelings/feel8.jpg',
                sense: 'listen',
                pleasant: true,
                intenese: 1
            },
            {
                // 9
                name: 'Lonely',
                price: 7200,
                url: 'img/feelings/feel9.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 10
                name: 'Calm',
                price: 900,
                url: 'img/feelings/feel10.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 1
            },
            {
                // 11
                name: 'Depressed',
                price: 7500,
                url: 'img/feelings/feel11.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 3
            },
            {
                // 12
                name: 'Relaxed',
                price: 1200,
                url: 'img/feelings/feel12.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 1
            },
            {
                // 13
                name: 'Alone',
                price: 8100,
                url: 'img/feelings/feel13.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 14
                name: 'Joy',
                price: 5400,
                url: 'img/feelings/feel14.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 2
            },
            {
                // 15
                name: 'Misgiving',
                price: 7700,
                url: 'img/feelings/feel15.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 1
            },
            {
                // 16
                name: 'Bright',
                price: 2900,
                url: 'img/feelings/feel16.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 3
            },
            {
                // 17
                name: 'Helpless',
                price: 700,
                url: 'img/feelings/feel17.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 18
                name: 'Quiet',
                price: 900,
                url: 'img/feelings/feel18.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 1
            },
            {
                // 19
                name: 'Afraid',
                price: 4100,
                url: 'img/feelings/feel19.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 20
                name: 'Loved',
                price: 9900,
                url: 'img/feelings/feel20.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 3
            },
            {
                // 21
                name: 'Sad',
                price: 6400,
                url: 'img/feelings/feel21.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 22
                name: 'Devoted',
                price: 8800,
                url: 'img/feelings/feel22.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 1
            },
            {
                // 23
                name: 'Crushed',
                price: 1200,
                url: 'img/feelings/feel23.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 24
                name: 'Fascinated',
                price: 3000,
                url: 'img/feelings/feel24.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 1
            },
            {
                // 25
                name: 'Grief',
                price: 6700,
                url: 'img/feelings/feel25.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 26
                name: 'Inquisitive',
                price: 400,
                url: 'img/feelings/feel26.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 2
            },
            {
                // 27
                name: 'Sorrowful',
                price: 5500,
                url: 'img/feelings/feel27.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 2
            },
            {
                // 28
                name: 'Brave',
                price: 8000,
                url: 'img/feelings/feel28.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 3
            },
            {
                // 29
                name: 'Lifeless',
                price: 5000,
                url: 'img/feelings/feel29.jpg',
                sense: 'visual',
                pleasant: false,
                intenese: 3
            },
            {
                // 30
                name: 'Hopeful',
                price: 10000,
                url: 'img/feelings/feel30.jpg',
                sense: 'visual',
                pleasant: true,
                intenese: 2
            }
        ]
    }

    return info;
}
