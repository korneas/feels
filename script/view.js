var view = {
    validation: [
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false
    ],

    preload: function preload() {
        setTimeout(function () {
            document.querySelector('#preloader').style.opacity = 0;
        }, 4000);
        setTimeout(function () {
            document.querySelector('#preloader').style.display = 'none';
        }, 5000);
        if ($(document).scrollTop() > 0) {
            window.scrollBy(0, 1);
            $(document).scrollTop(1);
        }
    },

    menuAction: function menuAction() {
        var menu = document.querySelector('section');
        menu.addEventListener('click', function () {
            if ($('#change').hasClass('active')) {
                $('#change').removeClass('active');
                console.log('menu-close');
            } else {
                $('#change').addClass('active');
                console.log('menu-open');
            }
        });
    },

    changeNav: function changeNav() {
        if ($(document).scrollTop() >= $('#ads').height()) {
            var header = document.querySelector('#header');
            var filters = document.querySelector('#filters');
            if (header.style.backgroundColor != '#191919') {
                header.style.backgroundColor = '#191919';
                filters.style.opacity = 1;
            }
        } else {
            var header = document.querySelector('#header');
            var filters = document.querySelector('#filters');
            header.style.backgroundColor = 'transparent';
            filters.style.opacity = 0;
        }

        if ($(document).scrollTop() >= $('#ads').height() + $('#newItem').height() - 100) {
            var filters = document.querySelector('#filters');
            filters.style.opacity = 1;
        } else {
            var filters = document.querySelector('#filters');
            filters.style.opacity = 0;
        }
    },

    getItems: function getItems(list) {
        var items = document.createElement('div');
        items.setAttribute('id', 'store');

        var that = this;

        var cont = document.createElement('div');
        cont.setAttribute('class', 'container flex-store');
        cont.innerHTML = '<div style="height:80px;width:1000px;"></div><br>';

        for (var i = 0; i < list.length; i++) {
            var square = document.createElement('div');
            square.setAttribute('class', 'itemSquare');

            square.innerHTML = `
                <div class="itemImg" style="background-image:url(${list[i].url})"></div>
                <p class="name">${list[i].name}\n</p>
                <p class="price">${list[i].price}</p>
                <button class="addItem">+</button>
            `;

            cont.appendChild(square);
        }

        cont.innerHTML += '<br><div style="height:80px;width:1000px;"></div><br>'
        items.appendChild(cont);

        items.querySelectorAll('.addItem').forEach(function (btn, index) {
            btn.addEventListener('click', function () {
                var modal = that.createModal(list[index]);
                items.appendChild(modal);
                $('body').addClass('stop-scrolling');
            });
        });

        console.log("Items created")
        return items;
    },

    getAdvertising: function getAdvertising(ads, rand) {
        var container = document.createElement('div');
        container.setAttribute('id', 'newItem');

        container.innerHTML = `
            <div style="background-image: url('${ads[rand].url}')" id="ad_img">
            <div class="ad-order"><h5>Pre-order</h5></div>
            <div class="ad-price"><h5>$ ${ads[rand].price}</h5></div>
            </div></div>
            <div class="container">
            <h4>New</h4>
            <h1>${ads[rand].name}</h1>
            <p>${ads[rand].texto}</p>
            </div>
        `;

        console.log("Ad created: " + rand);
        return container;
    },

    createInit: function createInit() {
        var init = document.createElement('div');
        init.innerHTML = `
        <div id="bg"></div>

        <div id="ads">
        <div class="container">
            <div id="principal"><p data-text="What you want to feel?">What you<br>want to<br>feel?</p></div>
        </div>
        </div>
        `;

        console.log('Init items');
        return init;
    },

    createModal: function createModal(infoFeel) {
        console.log('Crear modal');
        var div = document.createElement('div');

        div.innerHTML = `
        <div class="modal-backdrop fade"></div>
        <div class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">${infoFeel.name}</h4>
            </div>
            <div class="modal-body">
                <div style="background-image:url(${infoFeel.url})" class="modal-img"></div>
                <p class="modalPrice">Price: $${infoFeel.price}</p>
                <button class="buyModal">Buy</button>
                <p class="modal-article">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et aperiam eos consequuntur maiores delectus, voluptas ea veritatis rem error illo vero officiis.</p>
            </div>
          </div>
        </div>
        </div>
        `;
        
        var modal = div.querySelector('.modal');
        var backdrop = div.querySelector('.modal-backdrop');

        modal.style.display = 'block';

        setTimeout(function () {
            modal.style.opacity = 1;
            backdrop.style.opacity = .4;
            div.querySelector('.modal-dialog').style.transform = 'translate(0,0)';
        });

        var remove = function () {
            modal.style.opacity = 0;
            backdrop.style.opacity = 0;
            if ($('body').hasClass('stop-scrolling')) {
                $('body').removeClass('stop-scrolling');
            }
            setTimeout(function () {
                div.remove();
            }, 500);
        };

        modal.addEventListener('click', function (e) {
            if (e.target == modal) remove();
        });

        div.querySelector('button').addEventListener('click', remove);

        return div;
    },

    setHeaderEvent: function setHeaderEvent() {
        var header = document.querySelector('#filters');
        var that = this;

        var handler = function () {
            that.onFilter(that.validation);
        };

        //Validacion del corazon
        header.querySelector('#heart').addEventListener('click', function () {

            if (that.validation[0] == false) {
                that.validation[0] = true;
                that.validation[1] = false;
                handler();
            } else {
                that.validation[0] = false;
                handler();
            }
        });

        //Validacion del corazon roto
        header.querySelector('#unheart').addEventListener('click', function () {
            if (that.validation[1] == false) {
                that.validation[1] = true;
                that.validation[0] = false;
                handler();
            } else {
                that.validation[1] = false;
                handler();
            }
        });

        //Validacion del precio menor
        header.querySelector('#rcheap').addEventListener('click', function () {
            if (that.validation[2] == false) {
                that.validation[2] = true;
                that.validation[3] = false;
                that.validation[4] = false;
                that.validation[5] = false;
                handler();
            } else {
                that.validation[2] = false;
                handler();
            }
        });

        //Validacion del precio medio
        header.querySelector('#cheap').addEventListener('click', function () {
            if (that.validation[3] == false) {
                that.validation[3] = true;
                that.validation[2] = false;
                that.validation[4] = false;
                that.validation[5] = false;
                handler();
            } else {
                that.validation[3] = false;
                handler();
            }
        });

        //Validacion del precio alto
        header.querySelector('#expensive').addEventListener('click', function () {
            if (that.validation[4] == false) {
                that.validation[4] = true;
                that.validation[3] = false;
                that.validation[2] = false;
                that.validation[5] = false;
                handler();
            } else {
                that.validation[4] = false;
                handler();
            }
        });

        //Validacion del precio muy alto
        header.querySelector('#rexpensive').addEventListener('click', function () {
            if (that.validation[5] == false) {
                that.validation[5] = true;
                that.validation[3] = false;
                that.validation[4] = false;
                that.validation[2] = false;
                handler();
            } else {
                that.validation[5] = false;
                handler();
            }
        });

        //Validacion del visual
        header.querySelector('#visual').addEventListener('click', function () {
            if (that.validation[6] == false) {
                that.validation[6] = true;
                that.validation[7] = false;
                that.validation[8] = false;
                handler();
            } else {
                that.validation[6] = false;
                handler();
            }
        });

        //Validacion del escuchado
        header.querySelector('#ear').addEventListener('click', function () {
            if (that.validation[7] == false) {
                that.validation[7] = true;
                that.validation[6] = false;
                that.validation[8] = false;
                handler();
            } else {
                that.validation[7] = false;
                handler();
            }
        });

        //Validacion del tactil
        header.querySelector('#touch').addEventListener('click', function () {
            if (that.validation[8] == false) {
                that.validation[8] = true;
                that.validation[7] = false;
                that.validation[6] = false;
                handler();
            } else {
                that.validation[8] = false;
                handler();
            }
        });

        //Validacion de la intensidad baja
        header.querySelector('#low').addEventListener('click', function () {
            if (that.validation[9] == false) {
                that.validation[9] = true;
                that.validation[10] = false;
                that.validation[11] = false;
                handler();
            } else {
                that.validation[9] = false;
                handler();
            }
        });

        //Validacion de la intensidad media
        header.querySelector('#medium').addEventListener('click', function () {
            if (that.validation[10] == false) {
                that.validation[10] = true;
                that.validation[9] = false;
                that.validation[11] = false;
                handler();
            } else {
                that.validation[10] = false;
                handler();
            }
        });

        //Validacion de la intensidad alto
        header.querySelector('#high').addEventListener('click', function () {
            if (that.validation[11] == false) {
                that.validation[11] = true;
                that.validation[10] = false;
                that.validation[9] = false;
                handler();
            } else {
                that.validation[11] = false;
                handler();
            }
        });
    },

    setFilterEvent: function setFilterEvent() {
        var header = document.querySelector('#filters');
        var that = this;

        if (that.validation[0] == false) {
            header.querySelector('#heart').style.fill = '#FFF';
        } else {
            header.querySelector('#heart').style.fill = '#BD9C36';
        }

        if (that.validation[1] == false) {
            header.querySelector('#unheart').style.fill = '#FFF';
        } else {
            header.querySelector('#unheart').style.fill = '#BD9C36';
        }

        if (that.validation[2] == false) {
            header.querySelector('#rcheap').style.fill = '#FFF';
        } else {
            header.querySelector('#rcheap').style.fill = '#BD9C36';
        }

        if (that.validation[3] == false) {
            header.querySelector('#cheap').style.fill = '#FFF';
        } else {
            header.querySelector('#cheap').style.fill = '#BD9C36';
        }

        if (that.validation[4] == false) {
            header.querySelector('#expensive').style.fill = '#FFF';
        } else {
            header.querySelector('#expensive').style.fill = '#BD9C36';
        }

        if (that.validation[5] == false) {
            header.querySelector('#rexpensive').style.fill = '#FFF';
        } else {
            header.querySelector('#rexpensive').style.fill = '#BD9C36';
        }

        if (that.validation[6] == false) {
            header.querySelector('#visual').style.fill = '#FFF';
        } else {
            header.querySelector('#visual').style.fill = '#BD9C36';
        }

        if (that.validation[7] == false) {
            header.querySelector('#ear').style.fill = '#FFF';
        } else {
            header.querySelector('#ear').style.fill = '#BD9C36';
        }

        if (that.validation[8] == false) {
            header.querySelector('#touch').style.fill = '#FFF';
        } else {
            header.querySelector('#touch').style.fill = '#BD9C36';
        }

        if (that.validation[9] == false) {
            header.querySelector('#low').style.fill = '#FFF';
        } else {
            header.querySelector('#low').style.fill = '#BD9C36';
        }

        if (that.validation[10] == false) {
            header.querySelector('#medium').style.fill = '#FFF';
        } else {
            header.querySelector('#medium').style.fill = '#BD9C36';
        }

        if (that.validation[11] == false) {
            header.querySelector('#high').style.fill = '#FFF';
        } else {
            header.querySelector('#high').style.fill = '#BD9C36';
        }
    },

    render: function render(list, ads) {

        var main = document.querySelector('#main');
        var init = this.createInit();
        var random_ad = this.getAdvertising(ads, Math.floor(Math.random() * 3));
        var items = this.getItems(list);

        main.innerHTML = '';
        main.appendChild(init);
        main.appendChild(random_ad);
        main.appendChild(items);
    }
}
